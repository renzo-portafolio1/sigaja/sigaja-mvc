﻿using System;
using System.Collections.Generic;

namespace SIGAJA.Models
{
    public partial class Usuario
    {
        public int Id { get; set; }
        public string? Dni { get; set; }
        public string? Nombre { get; set; }
        public string? ApellidoPaterno { get; set; }
        public string? ApellidoMaterno { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public string? Sexo { get; set; }
    }
}
