
CREATE DATABASE SIGAJA_IEP
USE SIGAJA_IEP

CREATE TABLE Usuario(
Id int identity(1,1) primary key,
DNI varchar(50),
Nombre varchar(50),
ApellidoPaterno varchar(50),
ApellidoMaterno varchar(50),
FechaNacimiento date,
Sexo varchar(50)
)

select * from Usuario